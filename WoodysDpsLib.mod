<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="WoodysDpsLib" version="1.0" date="12/02/2008">		
    <Author name="Aerthan" email="aerthantn@gmail.com" />		
    <Description text="http://war.curseforge.com/projects/woodys_dps_lib/" />    
    <VersionSettings gameVersion="1.9.9" />		
    <Dependencies>			
      <Dependency name="EA_ChatWindow" />		
    </Dependencies>		
    <Files>			
      <File name="WoodysDpsLib.lua" />		
    </Files>		
    <OnInitialize>			
      <CallFunction name="WoodysDps.Initialize" />		
    </OnInitialize>		
    <OnUpdate>			
      <CallFunction name="WoodysDps.OnUpdate" />		
    </OnUpdate>	
  </UiMod>
</ModuleFile>