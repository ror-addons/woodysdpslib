WoodysDps = {
	timer = 0;
	combatStart = 0;
	damage = 0;
	damagePerSec = 0;
	totalCombatDuration = 0;
	baseDamage = 0;
	critDamage = 0;
	numHits = 0;
	numCrits = 0;
	damageAbilityTotals = {};
}

function WoodysDps.Initialize()
	-- Events
	RegisterEventHandler(SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED, "WoodysDps.OnPlayerCombatFlagUpdated" );
	RegisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "WoodysDps.OnWorldObjCombatEvt");

	WoodysDps.damageAbilityTotals["AA"] = 0;
end

function WoodysDps.OnUpdate(timePassed)
	WoodysDps.timer = WoodysDps.timer + timePassed;
end

function WoodysDps.CleanUp()
	UnregisterEventHandler(SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED, "WoodysDps.OnPlayerCombatFlagUpdated" );
	UnregisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "WoodysDps.OnWorldObjCombatEvt");
end

function WoodysDps.OnTimerReset()
	WoodysDps.damage = 0;
	WoodysDps.totalCombatDuration = 0;
	WoodysDps.baseDamage = 0;
	WoodysDps.critDamage = 0;
	WoodysDps.numHits = 0;
	WoodysDps.numCrits = 0;
	WoodysDps.damageAbilityTotals = {};
end

function WoodysDps.OnPlayerCombatFlagUpdated(arg)
	if (GameData.Player.inCombat)
	then
		-- set combatStart timestamp
		WoodysDps.combatStart = WoodysDps.timer;
	else
		if (WoodysDps.combatStart > 0)
		then
			WoodysDps.totalCombatDuration = WoodysDps.totalCombatDuration + WoodysDps.timer - WoodysDps.combatStart;
			WoodysDps.combatStart = 0;
		end
	end
	WoodysDps.damage = 0;
	WoodysDps.damagePerSec = 0;
end

function WoodysDps.OnWorldObjCombatEvt( targetObjNum, eventMagnitude , eventType, eventTrigger )
	d("targetObjNum:"..targetObjNum.." eventMagnitude:"..eventMagnitude.." eventType:"..eventType.." eventTrigger:"..eventTrigger);

	if (GameData.Player.inCombat)
	then
		local elapsedTime = WoodysDps.timer - WoodysDps.combatStart;
		if (eventMagnitude < 0 
			and targetObjNum ~= GameData.Player.worldObjNum
		    and (GameData.Player.Pet.objNum == 0 or targetObjNum ~= GameData.Player.Pet.objNum))
		then
			WoodysDps.damage = WoodysDps.damage + (-eventMagnitude);
		
			if (elapsedTime > 0)
			then
				WoodysDps.damagePerSec = WoodysDps.damage / elapsedTime;
			end
			
			if (eventType == 0 or eventType == 1)
			then
				WoodysDps.numHits = WoodysDps.numHits + 1;
				WoodysDps.baseDamage = WoodysDps.baseDamage + (-eventMagnitude);
			elseif (eventType == 2 or eventType == 3)
			then
				WoodysDps.numCrits = WoodysDps.numCrits + 1;
				WoodysDps.critDamage = WoodysDps.critDamage + (-eventMagnitude);
			end
			
			if ((eventType == 1 or eventType == 3) and eventTrigger > 0) -- add ability damage
			then
				local abilityName = GetAbilityName(eventTrigger);
				if (WoodysDps.damageAbilityTotals[abilityName] == nil)
				then
					WoodysDps.damageAbilityTotals[abilityName] = (-eventMagnitude);
				else
					WoodysDps.damageAbilityTotals[abilityName] = WoodysDps.damageAbilityTotals[abilityName] + (-eventMagnitude);
				end
			elseif (eventType == 0 or eventType == 2)
			then
				WoodysDps.damageAbilityTotals["AA"] = WoodysDps.damageAbilityTotals["AA"] + (-eventMagnitude);
			end
		end
	end
end


function WoodysDps.CreateBasicTooltip(name,anchor)
    Tooltips.CreateTextOnlyTooltip(name, nil);
    Tooltips.AnchorTooltip(anchor);

	for i = 1,10
	do
		Tooltips.SetTooltipColor( i, 1, 128, 255, 255 );
	end
    Tooltips.SetTooltipText( 1, 1, L"  DPS");
	local pos = 2;
	if (WoodysDps.totalCombatDuration > 0)
	then
		local overallDps = (WoodysDps.baseDamage + WoodysDps.critDamage) / WoodysDps.totalCombatDuration;
		Tooltips.SetTooltipText(pos, 1, L"За сеанс:");
		Tooltips.SetTooltipText(pos, 3, towstring(string.format("%.1f", overallDps)));
		pos = pos + 1;
	end
	
	Tooltips.SetTooltipText(pos, 1, L"Общий:");
	Tooltips.SetTooltipText(pos, 3, towstring(string.format("%10d", (WoodysDps.baseDamage+WoodysDps.critDamage))));
	pos = pos + 1;
	if ((WoodysDps.numHits + WoodysDps.numCrits) > 0) -- crit %
	then
		local critPrc = WoodysDps.numCrits / (WoodysDps.numHits + WoodysDps.numCrits) * 100;
		Tooltips.SetTooltipText(pos, 1, L"Крит %:");
		Tooltips.SetTooltipText(pos, 3, towstring(string.format("%.2f%%", critPrc)));
		pos = pos + 1;
	end
	if ((WoodysDps.baseDamage + WoodysDps.critDamage) > 0) -- base vs crit damage
	then
		local baseDmgPrc = WoodysDps.baseDamage / (WoodysDps.baseDamage + WoodysDps.critDamage) * 100;
		local critDmgPrc = WoodysDps.critDamage / (WoodysDps.baseDamage + WoodysDps.critDamage) * 100;
		Tooltips.SetTooltipText(pos, 1, L"Обычный:\nКриты:");
		Tooltips.SetTooltipText(pos, 3, towstring(string.format("%10d (%.2f%%)\n%10d (%.2f%%)", 
		                        WoodysDps.baseDamage, baseDmgPrc, WoodysDps.critDamage, critDmgPrc)));
		pos = pos + 1;
	end
	if ((WoodysDps.baseDamage + WoodysDps.critDamage) > 0)
	then
		local abilityHeadings = L"---";
		local abilityValues = L"";
		
		local abilityTotalsCopy = WoodysDps.damageAbilityTotals; -- get a local copy
		for i,k in ipairs(WoodysDps.SortByTotal(abilityTotalsCopy))
		do
			local v = abilityTotalsCopy[k];
			local abilityDmgPrc = v / (WoodysDps.baseDamage + WoodysDps.critDamage) * 100;
			abilityHeadings = abilityHeadings..L"\n"..towstring(k)..L":";
			abilityValues = abilityValues..L"\n"..towstring(string.format("%10d (%.2f%%)", v, abilityDmgPrc));
		end
		Tooltips.SetTooltipText(pos, 1, abilityHeadings);
		Tooltips.SetTooltipText(pos, 3, abilityValues);
		pos = pos + 1;
	end
	Tooltips.Finalize();
end

function WoodysDps.SortByTotal(abilityTotals) -- sort abilityTotals by value and return the keys in order
	local sortedKeys = {};
	for n in pairs(abilityTotals)
	do
		table.insert(sortedKeys, n);
	end
	table.sort(sortedKeys, function(a,b) return abilityTotals[a] > abilityTotals[b] end);
	return sortedKeys;
end

