local Dps = {
	timer = 0;
	displayTime = 1.0;
}

function Dps.Initialize()
	Dps.Waaagh = WaaaghBar.NewPlugin("WaaaghBarDps");
	Dps.Dps = Dps.Waaagh:NewElement("Dps", 75, L"Dps", { r = 255, g = 255, b = 255 }, 106, { abr = "dps", func = Dps.OnDpsSlash});

	-- Events
	WindowRegisterCoreEventHandler(Dps.Dps.name, "OnMouseOver", "WaaaghBar.Dps.OnMouseOver");
	Dps.UpdateDpsDisplay();
end

function Dps.OnDpsSlash(opt, val)
	d("OnDpsSlash opt:"..opt.." val:"..val);
end

function Dps.OnUpdate(timePassed)
	Dps.timer = Dps.timer + timePassed;
	if (Dps.timer > Dps.displayTime) -- update after waiting a full second
	then
		Dps.UpdateDpsDisplay();
		Dps.timer = 0;
	end
end

function Dps.UpdateDpsDisplay()
	Dps.Dps:SetText(towstring(string.format("%.1f", WoodysDps.damagePerSec)));
end

function Dps.OnMouseOver()
	local anchor = Tooltips.ANCHOR_WINDOW_BOTTOM;
    if WaaaghBar.GetTooltipsAnchor ~= nil
	then
		anchor = WaaaghBar.GetTooltipsAnchor();
	end
	WoodysDps.CreateBasicTooltip(WaaaghBar.Dps.Dps.name, anchor);
end

WaaaghBar.Dps = Dps;
