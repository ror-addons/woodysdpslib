<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">	
  <UiMod name="Wdy_WaaaghBar_DPS" version="1.0" date="12/02/2008">		
    <Author name="Aerthan" email="aerthantn@gmail.com" />		
    <Description text="WaaaghBar addon using WoodysDpsLib" />		
    <Dependencies>			
      <Dependency name="WaaaghBar" />			
      <Dependency name="WoodysDpsLib" />		
    </Dependencies>		
    <Files>			
      <File name="Wdy_WaaaghBar_DPS.lua" />		
    </Files>		
    <OnInitialize>			
      <CallFunction name="WaaaghBar.Dps.Initialize" />		
    </OnInitialize>		
    <OnUpdate>			
      <CallFunction name="WaaaghBar.Dps.OnUpdate" />		
    </OnUpdate>	
  </UiMod>
</ModuleFile>